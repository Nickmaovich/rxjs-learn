# 8 Observable creation helpers #
* of
* from
* range
* interval
* timer
* empty
* never
* throwError

# 7 utility operators: #
* tap
* isempty
* every
* count
* ignoreElements
* repeat
* repeatWhen

# 8 single output operators #
* first
* last
* min
* max
* elementAt
* find
* findIndex
* single

# 8 multiple output operators #
* skip
* skipLast
* skipWhile
* skipUntil
* take
* takeLast
* takeWhile
* takeUntil

# 3 distinct operators  #
* distinct
* distinctUntilChanged
* distinctUntilKeyChanged

# 5 buffering operators #
* bufferCount
* buffer
* bufferTime
* bufferToggle
* bufferWhen

# 5 windowing operators #
* windowCount
* window
* windowTime
* windowToggle
* windowWhen

# 3 sampling operators #
* sample
* audit
* throttle

# 8 error handling operators #
* catchError
* finalize
* onErrorResumeNext
* retry
* retryWhen
* throwIfEmpty
* timeout
* timeoutWith

# 7 multicast operators #
* multicast
* share
* shareReplay
* publish
* publishBehavior
* publishLast
* publishReplay

# 5 grouping obsevables operators #
* combineAll
* concatAll
* exhaust
* mergeAll
* withLatestFrom

# 6 grouping values operators #
* groupBy
* pairwise
* partition
* switchAll
* toArray
* zipAll

# 18 value transformation operators #
* concatMap
* concatMapTo
* defaultIfEmpty
* endWith
* startWith
* exhaustMap
* expand
* map
* mapTo
* scan
* mergeScan
* pluck
* reduce
* switchMap
* mergeMapTo
* switchMapTo
* materialize
* dematerialize

# 11 time, duration, & schedule operators #
* auditTime
* sampleTime
* observeOn
* subscribeOn
* debounce
* debounceTime
* delay
* delayWhen
* throttleTime
* timeInterval
* timestamp