import { interval, of, timer } from 'rxjs';
import { zipAll, map, tap } from 'rxjs/operators';
import observer from '../utils';

const source$ = interval(1).pipe(
	map(next => `1ms: ${next}`));

const source2$ = interval(100).pipe(
	map(next => `100ms: ${next}`));

const source3$ = timer(3000, 500).pipe(
	map(next => `3s -> 100ms: ${next}`)
);

of(source$, source2$, source3$).pipe(
	zipAll()
).subscribe(observer);