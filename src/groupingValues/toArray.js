import { interval } from 'rxjs';
import { tap, toArray } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	toArray()
).subscribe(observer);