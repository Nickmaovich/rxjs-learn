import { from } from 'rxjs';
import { partition, tap } from 'rxjs/operators';
import { observers$ } from '../utils';

const [obs1$, obs2$] = observers$;

const days = [
	{ name: 'MON', busy: true },
	{ name: 'TUE', busy: true },
	{ name: 'WED', busy: true },
	{ name: 'THU', busy: true },
	{ name: 'FRI', busy: true },
	{ name: 'SAT', busy: false },
	{ name: 'SUN', busy: false },
];

const [free, busy] = from(days).pipe(
	partition(next => !next.busy)
);

free.pipe(
	tap(next => {
		console.log('Free:', next);
	})
).subscribe(obs1$);

busy.pipe(
	tap(next => {
		console.log('Busy:', next);
	})
).subscribe(obs2$);