import { Subject, of } from 'rxjs';
import { delay, endWith, switchAll, tap } from 'rxjs/operators';
import observer from '../utils';

const source$ = of('A1', 'A2', 'A3', 'A4').pipe(
	tap(next => {
		console.log(next.indexOf('1') !== -1 ? 'start of A' : 'Emitted: ' + next);
	}),
	endWith('end of source$ (A)')
);

const source2$ = of('B1', 'B2').pipe(
	tap(next => {
		console.log(next.indexOf('1') !== -1 ? 'start of B' : 'Emitted: ' + next);
	}),
	delay(100),
	endWith('end of source2$ (B)')
);

const source3$ = of('C1', 'C2', 'C3').pipe(
	tap(next => {
		console.log(next.indexOf('1') !== -1 ? 'start of C' : 'Emitted: ' + next);
	}),
	endWith('end of source3$ (C)')
);

of(source$, source2$, source3$).pipe(
	switchAll()
).subscribe(observer);

//#region Example with Subject
// const origin = new Subject();
// origin.pipe(switchAll()).subscribe(observer);

// origin.next(source$);
// setTimeout(_ => origin.next(source2$), 1000);
// setTimeout(_ => origin.next(source3$), 1000);
//#endregion