import { from } from 'rxjs';
import { groupBy, mergeMap, reduce, toArray } from 'rxjs/operators';
import observer from '../utils';

const pullRequests = [
	{ id: 'profile-login-logout', comments: 3, merged: true },
	{ id: 'profile-login-logout', comments: 15, merged: true },
	{ id: 'speed-up-jest', comments: 1, merged: true },
	{ id: 'speed-up-jest', comments: 4, merged: true },
	{ id: 'remove-reactjs', comments: 9001, merged: false },
];

from(pullRequests).pipe(
	groupBy(t => t.id),
	mergeMap(group$ => group$.pipe(toArray()))
).subscribe(observer);

//#region With reduce
// from(pullRequests).pipe(
// 	groupBy(t => t.id),
// 	mergeMap(post =>
// 		post.pipe(
// 			reduce(
// 				(acc, curr) => {
// 					acc.id = acc.id || curr.id;
// 					acc.comments += curr.comments;
// 					acc.merged = acc.merged || curr.merged;
// 					return acc;
// 				},
// 				{ id: null, comments: 0, merged: false }
// 			)
// 		))
// ).subscribe(observer);
//#endregion