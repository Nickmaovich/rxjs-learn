import { interval, timer } from 'rxjs';
import { audit, tap, take } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	take(90),
	tap(next => {
		console.log('Emitted:', next);
	}),
	audit(next => {
		console.log('Used to calculate new observable:', next);
		return timer(next * 327);
	})
).subscribe(observer);