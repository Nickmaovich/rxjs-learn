import { interval } from 'rxjs';
import { take, tap, window } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	take(30),
	tap(next => {
		console.log('Emitted:', next);
	}),
	window(interval(350))
).subscribe(observer);