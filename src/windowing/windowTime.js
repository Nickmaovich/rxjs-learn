import { interval } from 'rxjs';
import { take, tap, windowTime } from 'rxjs/operators';
import observer from '../utils';

interval(500).pipe(
	take(10),
	tap(next => {
		console.log('Emitted:', next);
	}),
	windowTime(1000, 500)
).subscribe(observer);