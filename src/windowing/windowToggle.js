import { interval } from 'rxjs';
import { take, tap, windowToggle } from 'rxjs/operators';
import observer from '../utils';

const opening = interval(400).pipe(
	tap(_ => console.log('open')));

const closing = _ => interval(300).pipe(
	tap(_ => console.log('close')));

interval(100).pipe(
	tap(next => {
		console.log(next);
	}),
	windowToggle(opening, closing),
	take(3)
).subscribe(observer);