import { interval } from 'rxjs';
import { take, tap, windowCount, } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	take(30),
	tap(next => {
		console.log('Emitted:', next);
	}),
	windowCount(7)
).subscribe(observer);