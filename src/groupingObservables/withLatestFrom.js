import { of, timer } from 'rxjs';
import { delay, map, take, withLatestFrom } from 'rxjs/operators';
import observer from '../utils';

const source$ = timer(0, 400).pipe(
    take(10),
    map(next => `A${next}`)
);

const internal1$ = timer(0, 100).pipe(
    take(3),
    map(next => `B${next}`)
);

const internal2$ = of('C').pipe(delay(300));

source$.pipe(withLatestFrom(internal1$, internal2$)).subscribe(observer);