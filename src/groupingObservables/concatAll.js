import { of, from } from 'rxjs';
import { concatAll } from 'rxjs/operators';
import observer from '../utils';

const days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday'];
const weekends = ['Saturday', 'Sunday'];

of(from(days), from(weekends)).pipe(
	concatAll()
).subscribe(observer);