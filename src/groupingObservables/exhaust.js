import { of } from 'rxjs';
import { exhaust, tap, delay } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(1, 2, 3).pipe(
	delay(100)
);
const source2$ = of('this will never emit');

of(source$, source2$).pipe(
	exhaust(),
	tap(next => {
		console.log('', next);
	})
).subscribe(observer);