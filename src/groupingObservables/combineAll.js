import { of, from, timer } from 'rxjs';
import { combineAll } from 'rxjs/operators';
import observer from '../utils';

const source$ = of();
const source2$ = of(1, 2, 3);

of(source$, source2$).pipe(
	combineAll()
).subscribe(observer);

//#region all non empty
// const source3$ = of(1, 2, 3);
// const source4$ = from('Hello world');
// const source5$ = timer(1000);

// of(source3$, source4$, source5$).pipe(
// 	combineAll()
// ).subscribe(observer);
//#endregion