import { of, from, timer } from 'rxjs';
import { delay, mergeAll, take } from 'rxjs/operators';
import observer from '../utils';

const source$ = timer(0, 1).pipe(
	take(10)
);

const source2$ = from(['MON', 'TUE', 'WED', 'THU', 'FRI']).pipe(
	delay(5)
);

const source3$ = from(['SAT', 'SUN']).pipe(
	delay(5)
);

of(source$, source2$, source3$).pipe(
	mergeAll()
).subscribe(observer);