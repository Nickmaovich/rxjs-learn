import { interval } from 'rxjs';
import { tap, shareReplay, take } from 'rxjs/operators';
import { observers$ } from '../utils';
let colors = require('colors/safe');

const [obs1$, obs2$, obs3$, obs4$, obs5$] = observers$;

const observable$ = interval(750).pipe(
	tap(next => {
		console.log(colors.red('Emitted from source:--------------------'), next);
	}),
	take(22),
	shareReplay(2)
);

//#region subscribe with timeouts
observable$.subscribe(obs1$);
setTimeout(() => observable$.subscribe(obs2$), 3500);
setTimeout(() => observable$.subscribe(obs3$), 7000);
setTimeout(() => observable$.subscribe(obs4$), 10000);
setTimeout(() => observable$.subscribe(obs5$), 15000);
//#endregion