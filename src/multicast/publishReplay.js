import { range } from 'rxjs';
import { tap, publishReplay } from 'rxjs/operators';
import { observers$ } from '../utils';

const [obs1$, obs2$, obs3$] = observers$;

const connectable$ = range(1, 10).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	publishReplay(1));

connectable$.subscribe(obs1$);
connectable$.subscribe(obs2$);
connectable$.connect();

setTimeout(_ => connectable$.subscribe(obs3$), 3000);