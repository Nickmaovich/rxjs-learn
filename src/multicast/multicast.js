import { Subject, range } from 'rxjs';
import { tap, multicast } from 'rxjs/operators';
import { observers$ } from '../utils';
var colors = require('colors/safe');

const [obs1$, obs2$] = observers$;

const observable$ = range(1, 10).pipe(
	tap(next => {
		console.log(colors.red('Heavy side effect for---------------'), next);
	}));

observable$.subscribe(obs1$);
observable$.subscribe(obs2$);

//#region use multicast
const proxy$ = new Subject();
const connectableObservable$ = observable$.pipe(multicast(proxy$));

proxy$.subscribe(obs1$);
proxy$.subscribe(obs2$);

connectableObservable$.connect();
//#endregion