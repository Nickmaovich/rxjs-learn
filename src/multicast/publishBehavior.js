import { range } from 'rxjs';
import { tap, publishBehavior } from 'rxjs/operators';
import { observers$ } from '../utils';

const [obs1$, obs2$, obs3$] = observers$;

const connectable$ = range(1, 10).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	publishBehavior('Hello world.'));

connectable$.subscribe(obs1$);
connectable$.subscribe(obs2$);

setTimeout(() => connectable$.connect(), 3000);
//setTimeout(_ => connectable$.subscribe(obs3$), 5000);