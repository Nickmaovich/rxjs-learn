import { of } from 'rxjs';
import { tap, distinctUntilKeyChanged } from 'rxjs/operators';
import observer from '../utils';

const source$ = of({ age: 4, name: 'Foo' },
	{ age: 7, name: 'Bar' },
	{ age: 5, name: 'Foo' },
	{ age: 6, name: 'Foo' });

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	distinctUntilKeyChanged('name')
).subscribe(observer);