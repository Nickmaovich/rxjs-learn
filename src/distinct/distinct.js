import { of, interval } from 'rxjs';
import { tap, distinct } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(1, 2, 3, 4, 5, 6, 6, 7, 8, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	distinct()
).subscribe(observer);

//#region distinct interval
interval(100).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	distinct()
).subscribe(observer);
//#endregion