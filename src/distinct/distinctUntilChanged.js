import { of } from 'rxjs';
import { tap, distinctUntilChanged, } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(1, 1, 1, 1, 2, 2, 3, 1, 1, 2, 3, 4, 5);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	distinctUntilChanged()
).subscribe(observer);