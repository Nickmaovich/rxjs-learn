import { throwError } from 'rxjs';
import observer from '../utils';

throwError('Error from empty')
	.subscribe(observer);

//#region overloads
// throwError(new Promise(_ => { }))
// 	.subscribe(observer);

// throwError(1)
// 	.subscribe(observer);

// throwError([1, 2, 3, 4])
// 	.subscribe(observer);

// throwError({})
// 	.subscribe(observer);

// throwError(undefined)
// 	.subscribe(observer);

// throwError('rxjs')
// 	.subscribe(observer);
//#endregion