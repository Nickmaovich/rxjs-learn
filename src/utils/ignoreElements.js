import { of, interval, timer } from 'rxjs';
import { tap, ignoreElements } from 'rxjs/operators';
import observer from '../utils';

of(1, 2, 3).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	ignoreElements()
).subscribe(observer);

//#region what about interval?
// interval().pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	ignoreElements()
// ).subscribe(observer);
//#endregion

//#region what about timer?
// timer(5000).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	ignoreElements()
// ).subscribe(observer);
//#endregion