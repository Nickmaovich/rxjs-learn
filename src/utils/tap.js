import { interval } from 'rxjs';
import { tap } from 'rxjs/operators';
import observer from '../utils';

interval(500).pipe(
	tap(next => {
		console.log('Emitted:', next);
	})
).subscribe(observer);

//#region notes
//Google search on pipeable operators:
//https://github.com/ReactiveX/rxjs/blob/master/doc/pipeable-operators.md
//#endregion