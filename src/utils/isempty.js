import { interval, of, never } from 'rxjs';
import { isEmpty, tap } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	isEmpty()
).subscribe(observer);

//#region get true
// of().pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	isEmpty()
// ).subscribe(observer);
//#endregion

//#region question the existence 
// never().pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	isEmpty()
// ).subscribe(observer);

// setTimeout(_ => { }, Math.pow(2, 14));
//#endregion