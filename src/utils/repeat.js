import { of, } from 'rxjs';
import { repeat, tap } from 'rxjs/operators';
import observer from '../utils';

of(1, 2, 3).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	repeat(2)
).subscribe(observer);