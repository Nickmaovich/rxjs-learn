import { timer } from 'rxjs';
import observer from '../utils';

timer(10000)
	.subscribe(observer);

//#region emit at date
// const emitAt = new Date(Date.now() + 3500);
// timer(emitAt)
// 	.subscribe(observer);
//#endregion

//#region wait for N ms, then emit
// timer(3000, 100)
// 	.subscribe(observer);
//#endregion

//#region wait for date to come, then emit
// const startEmittingAt = new Date(Date.now() + 1000);

// timer(startEmittingAt, 200)
// 	.subscribe(observer);
//#endregion