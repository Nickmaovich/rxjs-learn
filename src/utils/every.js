import { interval } from 'rxjs';
import { tap, every } from 'rxjs/operators';
import observer from '../utils';

interval(10).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	every(item => item >= 0)
).subscribe(observer);