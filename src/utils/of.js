import { of } from 'rxjs';
import observer from '../utils';

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

of(numbers)
	.subscribe(observer);

//#region Make arrays great again
// of(...numbers)
// 	.subscribe(observer);
//#endregion

//#region Mind the args...
// of(1, 2, 3, 4, 5, 6, 7, 8, 9)
// 	.subscribe(observer);
//#endregion

//#region Whatever you want...
// of(1, 'rxjs', true, new Promise(_ => { }), {}, console.log, observer)
// 	.subscribe(observer);
//#endregion