import { range } from 'rxjs';
import observer from '../utils';

range(1, 10)
	.subscribe(observer);

//#region overloads
// range(100)
// 	.subscribe(observer);
//#endregion