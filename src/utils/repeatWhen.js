import { of, interval, timer } from 'rxjs';
import { repeatWhen, tap, take } from 'rxjs/operators';
import observer from '../utils';

of(1, 2, 3).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	repeatWhen(_ =>
		timer(5000, 3000).pipe(
			take(2)))
).subscribe(observer);