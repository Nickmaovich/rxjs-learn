import { of } from 'rxjs';
import { tap, count } from 'rxjs/operators';
import observer from '../utils';

of(1, 2, 3, 4, 5, 6, 7, 8, 9)
	.pipe(
		tap(next => {
			console.log('Emitted:', next);
		}),
		count())
	.subscribe(observer);

//#region count with predicate
// of(1, 2, 3, 4).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	count(item => item > 2)
// ).subscribe(observer);
//#endregion