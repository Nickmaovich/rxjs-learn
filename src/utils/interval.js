import { interval } from 'rxjs';
import observer from '../utils';

interval()
	.subscribe(observer);

//#region overloads
// interval(100)
// 	.subscribe(observer);
//#endregion