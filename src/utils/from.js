import { from, } from 'rxjs';
import observer from '../utils';

from(new Promise(resolve => resolve('Hello world!')))
	.subscribe(observer);

//#region from string
// from('Hello world!')
// 	.subscribe(observer);
//#endregion

//#region from array
// from([1, 2, 3, 4, 5, 6, 7, 8, 9])
// 	.subscribe(observer);
//#endregion

//#region not from anything tho
// from(1)
// 	.subscribe(observer);
//#endregion