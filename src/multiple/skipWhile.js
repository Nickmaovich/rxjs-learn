import { of, empty, interval } from 'rxjs';
import { tap, skipWhile, } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	skipWhile(arg => arg != 42)
).subscribe(observer);

//#region skipWhile < 90
// source$.pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	skipWhile(arg => arg < 90)
// ).subscribe(observer);
//#endregion