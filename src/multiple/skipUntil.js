import { empty, interval } from 'rxjs';
import { tap, skipUntil } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	skipUntil(interval(1000))
).subscribe(observer);

//#region skipUntil empty
// interval(100).pipe(
// 	tap(next => {
// 		console.log('Emitted: ', next);
// 	}),
// 	skipUntil(empty())
// ).subscribe(observer);
//#endregion