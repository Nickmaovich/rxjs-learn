import { of, empty, interval } from 'rxjs';
import { tap, takeLast, } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	takeLast(5))
	.subscribe(observer);

//#region take last 2
// of(1, 2, 3, 4).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	takeLast(2)
// ).subscribe(observer);
//#endregion

//#region take last 100
// interval(10).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	takeLast(100)
// ).subscribe(observer);
//#endregion