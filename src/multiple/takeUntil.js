import { empty, interval } from 'rxjs';
import { tap, takeUntil } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	takeUntil(interval(1000))
).subscribe(observer);

//#region takeUntil empty
// interval(100).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	takeUntil(empty())
// ).subscribe(observer);
//#endregion