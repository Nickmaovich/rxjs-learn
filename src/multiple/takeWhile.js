import { of, empty, interval } from 'rxjs';
import { tap, takeWhile, } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	takeWhile(arg => arg != 42)
).subscribe(observer);

//#region take while < 10
// source$.pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	takeWhile(arg => arg < 10)
// ).subscribe(observer);
//#endregion