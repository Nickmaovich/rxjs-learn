import { of, empty, interval } from 'rxjs';
import { tap, skip, } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	skip(5)
).subscribe(observer);

//#region another example
// source$.pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	skip(50)
// ).subscribe(observer);
//#endregion