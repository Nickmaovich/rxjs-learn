import { of, interval } from 'rxjs';
import { tap, skipLast, } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	skipLast(5)
).subscribe(observer);

//#region skipLast 2/4
// of(1, 2, 3, 4).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	skipLast(2)
// ).subscribe(observer);
//#endregion

//#region skipLast 100 in interval()
// interval(100).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	skipLast(100)
// ).subscribe(observer);
//#endregion