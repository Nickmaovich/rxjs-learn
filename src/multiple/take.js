import { of, interval } from 'rxjs';
import { take, tap } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	take(5)
).subscribe(observer);

//#region take much more
// interval(10).pipe(
// 	tap(next => console.log('Emitted:', next)),
// 	take(100)
// ).subscribe(observer);
//#endregion