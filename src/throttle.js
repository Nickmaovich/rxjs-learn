import { interval, timer } from 'rxjs';
import { throttle, tap, take } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	take(10),
	tap(next => {
		console.log('Emitted from source:', next);
	}),
	throttle(next => {
		console.log('Used to calculate observable: ' + next);
		return timer(next * 327);
	})
).subscribe(observer);