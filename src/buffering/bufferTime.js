import { interval } from 'rxjs';
import { take, tap, bufferTime } from 'rxjs/operators';
import observer from '../utils';

interval(500).pipe(
	take(10),
	tap(next => {
		console.log('Emitted:', next);
	}),
	bufferTime(1000, 500)
).subscribe(observer);