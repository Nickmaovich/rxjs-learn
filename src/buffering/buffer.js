import { interval } from 'rxjs';
import { take, tap, buffer } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	take(50),
	tap(next => {
		console.log('Emitted:', next);
	}),
	buffer(interval(350))
).subscribe(observer);