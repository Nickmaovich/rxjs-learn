import { interval } from 'rxjs';
import { take, tap, bufferCount } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	take(50),
	tap(next => {
		console.log('Emitted:', next);
	}),
	bufferCount(7)
).subscribe(observer);