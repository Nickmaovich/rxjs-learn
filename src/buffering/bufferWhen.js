import { interval } from 'rxjs';
import { take, tap, bufferWhen } from 'rxjs/operators';
import observer from '../utils';

let number = 0;

interval(100).pipe(
	take(50),
	tap(next => {
		number = next;
		console.log('Used for calc:', next);
	}),
	bufferWhen(() => {
		return number < 12 ? interval(700) :
			number < 30 ? interval(300) : interval(30);
	})
).subscribe(observer);