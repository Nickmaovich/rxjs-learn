import { interval } from 'rxjs';
import { take, tap, bufferToggle } from 'rxjs/operators';
import observer from '../utils';

const opening = interval(400).pipe(tap(_ => console.log('open')));
const closing = () => interval(300).pipe(tap(_ => console.log('close')));

interval(100).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	bufferToggle(opening, closing),
	take(3)
).subscribe(observer);