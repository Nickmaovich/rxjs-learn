const colors = require('colors/safe');

export const createObserver = (color) => {
	const resultColor = color || colors.green;

	return {
		next: next => console.log(resultColor('Received by subscriber:'), next),
		error: err => console.log(colors.red('Error received by subscriber:'), err),
		complete: _ => console.log(resultColor('Complete received by subscriber.'))
	};
};

//#region default exports
export const observers$ = [
	colors.green,
	colors.blue,
	colors.yellow,
	colors.magenta,
	colors.zebra].map(createObserver);

export default createObserver();
//#endregion