import { of, empty } from 'rxjs';
import { tap, elementAt, } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	elementAt(0)
).subscribe(observer);

//#region at 9
// source$.pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	elementAt(9)
// ).subscribe(observer);

//#endregion

//#region at 100
// OI WEI
// source$.pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	elementAt(100)
// ).subscribe(observer);
//#endregion