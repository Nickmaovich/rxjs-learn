import { of, empty, interval } from 'rxjs';
import { tap, find } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 17, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted: ', next);
	}),
	find(arg => arg == 3)
	// find(arg => arg > 12)
	// find(arg => arg > 100)
).subscribe(observer);

//#region 404 not found
// interval(100).pipe(
//     tap(next => console.log('Emitted:', next)),
//     find(arg => arg == 'complete')
// ).subscribe(observer)
//#endregion