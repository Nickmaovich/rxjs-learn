import { of, empty } from 'rxjs';
import { tap, single } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	single()
).subscribe(observer);

//#region other examples
// empty().pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	single()
// ).subscribe(observer);

// source$.pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	single(arg => arg > 3 && arg < 5)
// ).subscribe(observer);
//#endregion