import { of, empty, interval, Observable } from 'rxjs';
import { tap, first } from 'rxjs/operators';
import observer from '../utils';

const finite$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);
finite$.pipe(
	tap(next => {
		console.log('Emitted by finite:', next);
	}),
	first())
	.subscribe(observer);

//#region infinite observable
// const infinite$ = interval(100);
// infinite$.pipe(
// 	tap(next => {
// 		console.log('Emitted by infinite:', next);
// 	}),
// 	first())
// 	.subscribe(observer);
//#endregion

//#region empty observable
// const empty$ = empty();
// empty$.pipe(
// 	tap(next => {
// 		console.log('Emitted by empty:', next);
// 	}),
// 	first()
// ).subscribe(observer);
//#endregion

//#region "never" ending observable
// const infiniter$ = new Observable((observer) => {
// 	setTimeout(_ => observer.complete(), 999999);
// });

// infiniter$.pipe(
// 	tap(next => {
// 		console.log('Emitted by infinite:', next);
// 	}),
// 	first())
// 	.subscribe(observer);
//#endregion