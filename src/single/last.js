import { of, interval, Observable, empty } from 'rxjs';
import { tap, last } from 'rxjs/operators';
import observer from '../utils';

const finite$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

finite$.pipe(
	tap(next => {
		console.log('Emitted by finite:', next);
	}),
	last())
	.subscribe(observer);

//#region infinite observable
// const infinite$ = interval(100);
// infinite$.pipe(
// 	tap(next => {
// 		console.log('Emitted by infinite:', next);
// 	}),
// 	last()
// ).subscribe(observer);
//#endregion

//#region empty observable
// const empty$ = empty();
// empty$.pipe(
// 	tap(next => {
// 		console.log('Emitted by empty:', next);
// 	}),
// 	last()
// ).subscribe(observer);
//#endregion

//#region never ending observable
// const infiniter$ = new Observable((observer) => {
// 	setTimeout(() => observer.complete(), 999999);
// });

// infiniter$.pipe(
// 	tap(next => {
// 		console.log('Emitted by infinite:', next);
// 	}),
// 	last()
// ).subscribe(observer);
//#endregion