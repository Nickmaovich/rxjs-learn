import { of, empty, interval } from 'rxjs';
import { tap, findIndex } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(5, 3, 84, 15, 4, 42, 8, 2, 3, 45, 9);

source$.pipe(
	tap(next => {
		console.log('Emitted: ' + next);
	}),
	findIndex(arg => arg == 3)
	//findIndex(arg => arg > 1)
	//findIndex(arg => arg > 100)
).subscribe(observer);

//#region find in empty
// empty().pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	findIndex(arg => arg !== 'undefined')
// ).subscribe(observer);
//#endregion

//#region find in interval?
// interval(10).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	findIndex(arg => arg < 32768)
// );
//#endregion