import { interval, Observable } from 'rxjs';
import { tap, sample, take } from 'rxjs/operators';
import observer from '../utils';

interval(500).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	sample(interval(1679)),
	take(10)
).subscribe(observer);

//#region custom notifier
// const notifier = new Observable(observer => {
// 	setTimeout(() => observer.next(), 150);
// 	setTimeout(() => observer.next(), 350);
// 	setTimeout(() => observer.next(), 750);
// });

// interval(100).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 	}),
// 	sample(notifier),
// 	take(3)
// ).subscribe(observer);
//#endregion