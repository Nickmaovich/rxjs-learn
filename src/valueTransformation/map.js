import { range } from 'rxjs';
import { map } from 'rxjs/operators';
import observer from '../utils';

range(10).pipe(
	map(next => Math.pow(next, 3))
).subscribe(observer);