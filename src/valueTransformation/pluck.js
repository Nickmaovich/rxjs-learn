import { of } from 'rxjs';
import { pluck, tap } from 'rxjs/operators';
import observer from '../utils';

const source$ = of(
	{ age: 4, name: 'Foo', address: { state: 'CA' } },
	{ age: 7, name: 'Bar', address: { state: 'NY' } },
	{ age: 5, name: 'Foo', address: {} },
	{ age: 6, name: 'Foo' });

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	pluck('name')
	// pluck('address', 'state')
).subscribe(observer);