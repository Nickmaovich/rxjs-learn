import { range } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import observer from '../utils';

range(100).pipe(
	mapTo('X')
).subscribe(observer);