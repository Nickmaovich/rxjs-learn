import { of } from 'rxjs';
import { endWith, tap } from 'rxjs/operators';
import observer from '../utils';

const regularDays = ['MON', 'TUE', 'WED', 'THU', 'FRI'];
const coolDays = ['SAT', 'SUN'];

of(...regularDays).pipe(
	tap(next => {
		console.log('Emitted', next);
	}),
	endWith(...coolDays)
).subscribe(observer);