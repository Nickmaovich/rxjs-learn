import { of, interval, timer } from 'rxjs';
import { exhaustMap, tap, delay, take } from 'rxjs/operators';
import observer from '../utils';

of('A', 'B', 'C', 'D').pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	exhaustMap(next => {
		switch (next) {
		case 'A': return of(next, next.toLowerCase());
		case 'B': return of(1, 2, 3, 4);
		case 'C': of(next).pipe(delay(5));
			break;
		default:
			return interval(100).pipe(take(2));
		}
	})).subscribe(observer);