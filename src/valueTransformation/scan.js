import { from } from 'rxjs';
import { scan } from 'rxjs/operators';
import observer from '../utils';

from([',', ' welcome', ' home']).pipe(
	scan((acc, val) => acc + val, 'heederz')
).subscribe(observer);