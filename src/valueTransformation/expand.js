import { empty, of } from 'rxjs';
import { expand } from 'rxjs/operators';
import observer from '../utils';

of(1).pipe(
	expand(next => next < 5 ? of(next + 1) : empty())
).subscribe(observer);