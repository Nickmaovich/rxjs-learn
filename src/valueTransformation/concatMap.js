import { interval, of } from 'rxjs';
import { concatMap, delay, tap } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	concatMap(x => [x, x * x])
).subscribe(observer);

//#region concatMap to delay
// of(10, 1000, 5000, 10000).pipe(
// 	concatMap(ms =>
// 		of(`Emitted after ${ms}, at ${new Date(Date.now())}`).pipe(
// 			delay(ms)))
// ).subscribe(observer);
//#endregion