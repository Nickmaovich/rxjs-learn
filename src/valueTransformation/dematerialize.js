import { Notification, of } from 'rxjs';
import { dematerialize, tap } from 'rxjs/operators';
import observer from '../utils';

of(Notification.createNext(0), Notification.createNext(1)).pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    dematerialize()
).subscribe(observer);