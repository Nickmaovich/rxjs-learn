import { of } from 'rxjs';
import { mergeScan } from 'rxjs/operators';
import observer from '../utils';

of(1, 2, 3).pipe(
	mergeScan((acc, val) => of('a', 'b', acc + val), 0)
).subscribe(observer); 