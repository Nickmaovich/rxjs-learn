import { interval, of } from 'rxjs';
import { switchMapTo, take } from 'rxjs/operators';
import observer from '../utils';

of(1, 2).pipe(
    switchMapTo(interval(100).pipe(take(2)))
).subscribe(observer);