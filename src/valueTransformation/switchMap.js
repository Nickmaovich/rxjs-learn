import { Observable, interval } from 'rxjs';
import { switchMap, take, tap } from 'rxjs/operators';
import observer from '../utils';

const character = new Observable(observer => {
	observer.next('A');
	setTimeout(_ => observer.next('B'), 200);
	setTimeout(_ => observer.next('C'), 500);
	setTimeout(_ => observer.complete(), 100);
});

const internal = interval(50).pipe(take(5));

character.pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	switchMap(() => internal)
).subscribe(observer);