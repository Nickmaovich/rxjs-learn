import { of, interval } from 'rxjs';
import { mergeMapTo, tap } from 'rxjs/operators';
import observer from '../utils';

of(1, 2).pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    mergeMapTo(of('some', 'inner', 'observable')),
    // mergeMapTo(interval(100)),
).subscribe(observer);

//#region With result selector
// of(1, 2).pipe(
//     tap(next => {
//         console.log('Emitted:', next);
//     }),
//     mergeMapTo(
//         of('some', 'inner', 'observable'),
//         (source, internal) => {
//             return `${source}: ${internal}`
//         }
//     )
// ).subscribe(observer);
//#endregion