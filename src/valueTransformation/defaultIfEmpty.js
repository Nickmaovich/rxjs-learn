import { empty } from 'rxjs';
import { defaultIfEmpty } from 'rxjs/operators';
import observer from '../utils';

empty().pipe(
	defaultIfEmpty('Hello world')
).subscribe(observer);