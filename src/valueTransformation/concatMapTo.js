import { from, interval } from 'rxjs';
import { concatMapTo, take, tap } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
	take(2),
	tap(next => {
		console.log('interval emitted', next);
	}),
	concatMapTo(from('Hi'))
).subscribe(observer);