import { interval } from 'rxjs';
import { materialize, tap, take } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    take(50),
    materialize(),
).subscribe(observer);