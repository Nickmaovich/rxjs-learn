import { of } from 'rxjs';
import { reduce } from 'rxjs/operators';
import observer from '../utils';

of(1, 2, 3).pipe(
	reduce((acc, val) => acc + val, 0)
).subscribe(observer);