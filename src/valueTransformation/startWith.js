import { of } from 'rxjs';
import { startWith, tap } from 'rxjs/operators';
import observer from '../utils';

of(3).pipe(
	tap(next => {
		console.log('Emitted', next);
	}),
	startWith(1, 2)
).subscribe(observer);