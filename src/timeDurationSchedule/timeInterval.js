import { interval } from 'rxjs';
import { tap, timeInterval } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    timeInterval()
).subscribe(observer);