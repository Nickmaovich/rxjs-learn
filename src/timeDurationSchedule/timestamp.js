import { interval } from 'rxjs';
import { tap, timestamp } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    timestamp()
).subscribe(observer);