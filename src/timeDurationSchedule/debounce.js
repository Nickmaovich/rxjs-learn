import { interval, timer, from } from 'rxjs';
import { debounce, tap } from 'rxjs/operators';
import observer from '../utils';

from('Hello world').pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    debounce(_ => timer(1000))
).subscribe(observer);

//#region 
// interval(1000).pipe(
//     tap(next => {
//         console.log('Emitted:', next);
//     }),
//     debounce(next => timer(next * 200))
// ).subscribe(observer);
//#endregion