import { Observable, interval, timer, from } from 'rxjs';
import { debounceTime, tap } from 'rxjs/operators';
import observer from '../utils';

from('Hello world').pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    debounceTime(1000)
).subscribe(observer);

//#region custom notifier
// const events = Observable.create(observer => {
//     observer.next(0);
//     setTimeout(() => observer.next(1), 550);
//     setTimeout(() => observer.next(2), 1000);
//     setTimeout(() => observer.complete(), 2000);
// });

// events.pipe(
//     tap(next => {
//         console.log('Emitted:', next);
//     }),
//     debounceTime(500)
// ).subscribe(observer);
//#endregion