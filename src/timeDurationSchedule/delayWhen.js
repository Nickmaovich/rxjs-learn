import { timer, interval } from 'rxjs';
import { delayWhen, tap } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    delayWhen(next => {
        return timer(next * 1000)
    })
).subscribe(observer);