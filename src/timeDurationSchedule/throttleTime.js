import { timer } from 'rxjs';
import { tap, take, throttleTime } from 'rxjs/operators';
import observer from '../utils';

timer(0, 500).pipe(
    take(100),
    tap(next => {
        console.log('Emitted:', next);
    }),
    throttleTime(700)
).subscribe(observer);