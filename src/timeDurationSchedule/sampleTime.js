import { interval, Observable } from 'rxjs';
import { tap, sampleTime, take } from 'rxjs/operators';
import observer from '../utils';

interval(100).pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    sampleTime(50),
    take(3)
).subscribe(observer);

//#region custom notifier
// const notifier$ = new Observable(observer => {
//     setTimeout(() => observer.next(0), 150);
//     setTimeout(() => observer.next(1), 350);
//     setTimeout(() => observer.next(2), 750);
//     setTimeout(() => observer.complete(), 5000);
// });

// notifier$.pipe(
//     tap(next => {
//         console.log('Emitted:', next);
//     }),
//     sampleTime(50),
// ).subscribe(observer);
//#endregion