import { of, asyncScheduler, asapScheduler } from 'rxjs';
import { tap, observeOn } from 'rxjs/operators';
import observer from '../utils';

// observeOn - re-emit value on a new scheduler

of('asap').pipe(
    tap(next => {
        console.log('Queued:', next);
    }),
    observeOn(asapScheduler)
).subscribe(observer);

of('async').pipe(
    tap(next => {
        console.log('Queued:', next);
    }),
    observeOn(asyncScheduler)
).subscribe(observer);

of('immediate').pipe(
    tap(next => {
        console.log('Queued:', next);
    }),
).subscribe(observer);