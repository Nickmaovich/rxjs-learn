import { of } from 'rxjs';
import { delay, tap } from 'rxjs/operators';
import observer from '../utils';

of('hello', 'world').pipe(
    tap(next => {
        console.log('Emitted:', next);
    }),
    delay(5000)
).subscribe(observer);