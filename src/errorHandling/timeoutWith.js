import { from, Observable, } from 'rxjs';
import { tap, timeoutWith } from 'rxjs/operators';
import observer from '../utils';

const source$ = new Observable(observer => {
	observer.next('A');
	setTimeout(_ => observer.next('B'), 100);
	setTimeout(_ => observer.next('C'), 300);
	setTimeout(_ => observer.complete(), 600);
});

const fallback$ = from('abc');

source$.pipe(
	tap(next => console.log('Emitted:', next)),
	timeoutWith(250, fallback$)
).subscribe(observer);

//#region with date
// source$.pipe(
// 	tap(next => console.log('Emitted:', next)),
// 	timeoutWith(new Date(Date.now() + 500), fallback$)
// ).subscribe(observer);
//#endregion