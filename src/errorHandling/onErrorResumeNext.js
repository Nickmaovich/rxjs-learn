import { of, } from 'rxjs';
import { onErrorResumeNext, tap } from 'rxjs/operators';
import observer from '../utils';

const source$ = of('there', 'is', 'no', 'spoon');
const backup$ = of('all', 'your', 'base', 'are', 'belong', 'to', 'us');

source$.pipe(
	tap(next => {
		console.log('Emitted:', next);
		if (next == 'no')
			throw new Error('wtf dude');
	}),
	onErrorResumeNext(backup$)
).subscribe(observer);