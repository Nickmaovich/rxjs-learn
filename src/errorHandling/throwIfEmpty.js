import { empty, } from 'rxjs';
import { tap, throwIfEmpty } from 'rxjs/operators';
import observer from '../utils';

empty().pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	throwIfEmpty()
).subscribe(observer);