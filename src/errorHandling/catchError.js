import { of, } from 'rxjs';
import { tap, catchError } from 'rxjs/operators';
import observer from '../utils';

of('test', 1).pipe(
	tap(next => {
		console.log('Emitted:', next.toUpperCase());
	}))
	.subscribe(observer);

//#region handle the error
// of('test', 1).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next.toUpperCase());
// 	}),
// 	catchError(_ => of(undefined))
// ).subscribe(observer);
//#endregion