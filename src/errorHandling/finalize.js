import { of, } from 'rxjs';
import { finalize, tap } from 'rxjs/operators';
import observer from '../utils';

of(1).pipe(
	tap(next => {
		console.log('Emitted:', next);
	}),
	finalize(_ => console.log('Finalizing some stuff'))
).subscribe(observer);

//#region finalize when error
// of('a', 1).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 		next.toUpperCase();
// 	}),
// 	finalize(_ => console.log('Finalizing some stuff'))
// ).subscribe(observer);
//#endregion