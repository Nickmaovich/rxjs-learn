import { of, } from 'rxjs';
import { retry, tap } from 'rxjs/operators';
import observer from '../utils';

of('a', 1).pipe(
	tap(next => {
		console.log('Emitted:', next);
		next.toUpperCase();
	}),
	retry(3)
).subscribe(observer);