import { interval } from 'rxjs';
import { retryWhen, scan, takeWhile, tap } from 'rxjs/operators';
import observer from '../utils';

interval(500).pipe(
	tap(next => {
		console.log('Emitted:', next);
		if (next == 2)
			throw 'WTF no twos here';
	}),
	retryWhen(errors => {
		return errors.pipe(
			tap(next => {
				console.log('Error:', next);
			}),
			scan(acc => acc + 1, 0),
			tap(retryCount => {
				if (retryCount === 2)
					console.log('Swallowing error and completing');
				else
					console.log('Ok lets do one more retry');
			}),
			takeWhile(errCount => errCount < 2));
	})
).subscribe(observer);

//#region Do not swallow
// interval(500).pipe(
// 	tap(next => {
// 		console.log('Emitted:', next);
// 		if (next == 2)
// 			throw 'WTF no twos here';
// 	}),
// 	retryWhen(errors => errors.pipe(
// 		tap(next => console.log('Error:', next)),
// 		scan(acc => acc + 1, 0),
// 		tap(retryCount => {
// 			if (retryCount === 2) {
// 				console.log('Nah screw this, have an error!');
// 				throw 'I tried twice but it didnt work out';
// 			}
// 			else
// 				console.log('Ok lets do one more retry');
// 		})
// 	))
// ).subscribe(observer);
//#endregion