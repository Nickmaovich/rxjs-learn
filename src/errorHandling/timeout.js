import { Observable, } from 'rxjs';
import { tap, timeout } from 'rxjs/operators';
import observer from '../utils';

const source$ = new Observable(observer => {
	observer.next('A');
	setTimeout(_ => observer.next('B'), 100);
	setTimeout(_ => observer.next('C'), 300);
	setTimeout(_ => observer.complete(), 600);
});

source$.pipe(
	tap(next => console.log('Emitted:', next)),
	timeout(350)
).subscribe(observer);

//#region with date
// source$.pipe(
// 	tap(next => console.log('Emitted:', next)),
// 	timeout(new Date(Date.now() + 500))
// ).subscribe(observer);
//#endregion