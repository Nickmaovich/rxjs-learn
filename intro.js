// DOM events

div.addEventListener('click', _ => {
    console.log('Clicked');
});

$div.on('click', _ => {
    console.log('Clicked');
});



















// Iteration callbacks

[1, 2, 3, 4, 5].map(a => a * a);
[1, 2, 3, 4, 5].filter(a => a > 15);






// Promise
let promise = new Promise((resolve, reject) => {
    //...
});

promise.then(
    _ => console.log('Success ' + arg),
    _ => console.log('Fail ' + arg));




//Streams
const readableStream = getStream();
readableStream.on('data', _ => console.log('Data'));
readableStream.on('end', _ => console.log('Completed'))
readableStream.on('err', _ => console.log('Error'))







//Common:
function next(data) { console.log(data) };
function complete() { console.log('Completed') };
function error(err) { console.log('Error:' + err) };

startDataStream(next, error, complete);